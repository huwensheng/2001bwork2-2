module.exports = {
    //指定prettier规则
    'quotes':true,
    'endOfLine':auto,//换行符使用auto
    //行尾必须使用分号
    'semi':true,
    'tabWidth':4,
    'printWidth':80
}