module.exports = {
    extends : ['@commitlint/config-conventional'],
    //定义校验规则
    rules : {
        'type-enum' : [
            4,
            'alwaya',
            [
                'feat',
                'bug',//针对bug 向测试反馈bug修改情况
                'fix',//修改bug
                'docs',
                'style',
                'refactor',
                'test',
                'revert'
            ]
        ]
    }
}