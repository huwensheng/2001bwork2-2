module.exports = {
    env: {
        browser: true,
        es2021: true,
        node:true,
        'vue/setup-compiler-macros':true
    },
    extends: [
        'eslint:recommended',
        'plugin:vue/vue3-recommended',
        "plugin:prettier/recommended",
        "plugin:@typescript-eslint/recommended",
        'standard'
    ],
    overrides: [],
    parser:"vue-eslint-parser",
    parserOptions: {
        ecmaVersion: 'latest',
        parser:'@typescript-eslint/parser',
        sourceType: 'module'
    },
    plugins: ['vue','@typescript-eslint'],
    rules: {
        semi:'off',
        'comma-dangle':'off',
        'vue/multi-word-component-names': 'off',
        '@typescript-eslint/no-var-requires':'off'
    }
}


// eslint-config-prettier 解决eslint中样式规范和prettier中的样式规范冲突时 以prettier为准
// eslint-plugin-prettier 将prettier作为eslint的规范来使用
