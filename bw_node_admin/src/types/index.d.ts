declare module '@smallwei/avue'

// 表格页面传参
export interface ColumnType {
    label: string;
    props: string;
    width?: number | string;
    type?: string;
    fixed?: string;
    actions?: {
      name: string;
      type?: string;
      icon?: string;
      event: (data: any) => void;
    }[];
    render?: (params: any) => string;
    rounce?: string;
    key?: number;
}

