export interface ColumnType {
  label: string;
  props: string;
  width?: number | string;
  type?: string;
  fixed?: string;
  actions?: {
    name: string;
    type?: string;
    icon?: string;
    event: (data: any) => void;
  }[];
  render?: (params: any) => string;
}
export interface tableType {
    current: number;
    size: number;
  }
  export interface systemType {
    id: number;
    time: number;
    createDate: string;
    ip: string;
    method: string;
    operation: string;
    params: string;
    username: string;
  }
  export interface systemResType {
    current: number;
    pages: number;
    records: systemType[];
    searchCount: boolean;
    size: number;
    total: number;
  }
  