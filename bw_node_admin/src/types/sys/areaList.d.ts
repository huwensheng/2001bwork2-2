export interface AddressType {
    areaId: number;
    areaName: string;
    areas: string | null;
    level: number;
    parentId: number;
  }
  export interface RenderAddress extends AddressType {
    children?: RenderAddress[];
  }
