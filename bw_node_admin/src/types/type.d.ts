export interface ResponseData<T> {
    status: number,
    statusText: string,
    data: T
}

export interface LoginParams {
    access_token: string,
    token_type: string,
    refresh_token: string,
    authorities: any[],
    shopId: number,
    expires_in: number,
    userId: number
}

export interface userInfo {
    createTime : string,
    email : string,
    mobile : string,
    roleIdList : Array<number>,
    shopId : number,
    status : number,
    userId : number,
    username : string
}

export interface NavList{
    authorities : Array<AuthorizatiesItem>,
    menuList : Array<NavListItem>
}

export interface AuthorizatiesItem {
    authority : string
}

export interface NavListItem {
    menuId : number,
    name : string,
    orderNum : number,
    parentName : string,
    perms : string,
    type : number,
    url : string,
}

export interface OrderListRes {
    current:number,
    pages:number,
    searchCount:boolean,
    size:number,
    total:number,
    records:any
}

export interface OrderListItem {

}
