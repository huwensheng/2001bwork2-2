import axios, { AxiosRequestConfig, AxiosInstance } from "axios"

import { ElLoading, ElMessage } from 'element-plus'

import { LoadingInstance } from "element-plus/es/components/loading/src/loading"

import localSto from './util'

// 定义是否需要显示loadding效果

const DEFAULT_LOADING = true

interface HttpRequestConfig extends AxiosRequestConfig {
    isShowLoading?: boolean
}

class HttpRequest {
    instance: AxiosInstance
    isShowLoading: boolean
    isLoadding?: LoadingInstance
    constructor(config: HttpRequestConfig) {
        this.instance = axios.create(config) //创建axios实例
        this.isShowLoading = config.isShowLoading || DEFAULT_LOADING
        // 添加请求拦截器
        this.instance.interceptors.request.use(configs => {
            // 判断是否需要加载loadding效果
            if (this.isShowLoading) {
                // 在发送请求之前显示loading效果
                this.isLoadding = ElLoading.service({
                    lock: true,
                    text: '正在请求中...',
                    background: 'rgba(0,0,0,6)'
                })
            }
            this.isShowLoading = false
            // 获取token放在请求头中
            const token = localSto.getCache('token')
            return {
                ...configs,
                headers: {
                    ...configs.headers,
                    Authorization: token
                }
            }
        }, (error) => {
            return Promise.reject(error)
        })
        // 添加响应拦截器 在服务器响应之后 关闭loading效果
        this.instance.interceptors.response.use(response => {
            // 关闭loading效果
            this.isLoadding?.close()
            return response
        }, error => {
            this.isLoadding?.close()
            // 针对后端响应的http状态码处理
            const { code } = error.response
            switch (code) {
                case 400:
                    ElMessage.error('请求失败')
                    break
                case 401:
                    ElMessage.error('没有访问权限')
                    break
            }
            return Promise.reject(error)
        })
    }

    request<T>(config: HttpRequestConfig): Promise<T> {
        return new Promise((resolve, reject) => {
            this.instance
                .request<any, T>(config)
                .then(res => {
                    // 更改isShowLoading的值
                    this.isShowLoading = DEFAULT_LOADING;
                    resolve(res)
                })
                .catch(error => {
                    this.isShowLoading = DEFAULT_LOADING
                    reject(error)
                    return error
                })
        })
    }

    get<T>(config: HttpRequestConfig): Promise<T> {
        return this.request<T>({
            ...config,
            method: 'GET'
        })
    }

    post<T>(config: HttpRequestConfig): Promise<T> {
        return this.request<T>({
            ...config,
            method: 'POST'
        })
    }

    delete<T> (config:HttpRequestConfig):Promise<T> {
        return this.request<T>({
            ...config,
            method:'DELETE'
        })
    }

    put<T> (config:HttpRequestConfig):Promise<T> {
        return this.request<T>({
            ...config,
            method:'PUT'
        })
    }
}


export default HttpRequest