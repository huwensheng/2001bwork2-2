import type { RouteRecordRaw } from "vue-router"

export const getRoutes = ( menuList:any ) => {
    // 最终返回的路由
    const routes:RouteRecordRaw[] = []

    // 所有的路由
    let allRoutes:RouteRecordRaw[] = []

    // 获取router文件里面的路由
    const routerFiles:any = import.meta.globEager('../router/module/*.ts')

    allRoutes = Object.keys(routerFiles).reduce((prev : any,next : string) => {
        prev.push(...routerFiles[next].default);
        return prev
    },[])

    console.log(allRoutes);
    
    const changeRoutes = ( menu:any ) => {
        for(const item of menu){
            // 判断type类型
            if(item.type === 1) {
                const route = allRoutes.find(val => val.path === '/'+item.url);
                if(route) routes.push(route)
            }else{
                changeRoutes(item.list)
            }
        }
    }
    changeRoutes(menuList)
    console.log(routes);
    return routes
}