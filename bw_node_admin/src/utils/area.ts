import {AddressType,RenderAddress} from '../types/sys/areaList.d'

export function treeDataTranslate(data: AddressType[]) {
    interface tempType {
      (arg0: string): AddressType[];
    }
    let res: RenderAddress[] = data.filter(
      (item: AddressType) => item.level === 1
    );
    res.forEach((item) => { 
      item.children = data.filter(
        (a: AddressType) => a.level === 2 && a.parentId === item.areaId
      );
      item.children.forEach((v: RenderAddress) => {
        v.children = data.filter(
          (a: AddressType) => a.level === 3 && a.parentId === v.areaId
        );
      });
    });
    console.log(res);
    return res;
  }