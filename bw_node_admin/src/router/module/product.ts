const produList = [
    {
        path:"/prod/prodTag",//分组管理
        name:"prod:prodtag",
        component:()=>import("../views/home/prod/prodTag.vue")
    },
    {
        path:"/prod/prodList",//产品管理
        name:"prod:prodlist",
        component:()=>import("../../views/home/prod/prodList.vue")
    },
    {
        path:"prod/category",//分类管理
        name:"prod:category",
        component:()=>import("../../views/home/prod/category.vue")
    },
    {
        path:"prod/prodComm",//评论管理
        name:"prod:prodcomm",
        component:()=>import("../../views/home/prod/prodComm.vue")
    },
    {
        path:"prod/spec",//规格管理
        name:"prod:spec",
        component:()=>import("../../views/home/prod/spec.vue")
    }
]

export default produList