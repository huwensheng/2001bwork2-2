const shop = [
    {
        path:"/shop/notice",//公告管理
        name:"shop:notice",
        component:()=>import("../../views/home/shop/notice.vue")
    },
    {
        path:"/shop/hotSearch",//热搜管理
        name:"shop:hotsearch",
        component:()=>import("../../views/home/shop/hotSearch.vue")
    },
    {
        path:"/admin/indexImg",//轮播图管理
        name:"admin:indeximg",
        component:()=>import("../../views/home/shop/indexImg.vue")
    },
    {
        path:"/shop/transport",//运费模板
        name:"shop:transport",
        component:()=>import("../../views/home/shop/transport.vue")
    },
    {
        path:"/shop/pickAddr",//自提点管理
        name:"shop:pickaddr",
        component:()=>import("../../views/home/shop/pickAddr.vue")
    }
]

export default shop