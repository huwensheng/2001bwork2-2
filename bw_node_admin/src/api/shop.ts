import request from '../utils/index'

//公告数据

export const get_notice=(query?:any)=>{
     return request.get({
        url:`/api/shop/notice/page`,
        params:query
    })
}

//公告搜索
export const getShopList = (params: any) =>
request.get({
    url: `/api/shop/notice/page`,
    method:'GET',
    params,
});

//公告删除
export const DelshopList = (id: any) =>
    request.delete({
        url: `/api/shop/notice/${id}`,
        method:'DELETE',
    });

//公告新增
export const _addnoticeList=(data:any)=>{
    return request.post({
        url:'/api/shop/notice',
        data
    })
}

//公告编辑
export const _editnoticeList=(data:any)=>{
    return request.put({
        url:'/api/shop/notice',
        data
    })
}

//热搜数据
export const shopHotList=(query?:any)=>{
    return request.get({
    url:`/api/admin/hotSearch/page`,
    method:'GET',
    params:query
})
}

//热搜删除
export const DelhotList=(data:any)=>{
    return request.delete({
        url:`/api/admin/hotSearch`,
        method:'DELETE',
        data
    })
}

export const TanrList=(data:any)=>{
    return request.delete({
        url:`/api/shop/transport`,
        method:'DELETE',
        data
    })
}

//热搜新增
export const _addhotList=(data:any)=>{
    return request.post({
        url:'/api/admin/hotSearch',
        data
    })
}

//热搜编辑
export const _edithotList=(data:any)=>{
    return request.put({
        url:'/api/admin/hotSearch',
        data
    })
}

//轮播数据
export const indexList=(query?:any)=>{
    return request.get({
    url:`/api/admin/indexImg/page`,
    method:'GET',
    params:query
})
}

//轮播选择商品数据
export const swiperlist=(query?:any)=>{
   return request.get({
      url:`/api/prod/prod/page`,
      method:'GET',
      params:query
   })
}

//轮播删除
export const DelIndexList=(data:any)=>{
    return request.delete({
        url:`/api/admin/indexImg`,
        method:'DELETE',
        data
    })
}

//轮播新增
export const swiperadd=(data:any)=>{
    return request.post({
        url:'/api/admin/indexImg',
        data
    })
}

//轮播编辑
export const swiperedit=(data:any)=>{
    return request.put({
        url:'/api/admin/indexImg',
        data
    })
}

//运费数据
export const _transportList=(query?:any)=>{
   return request.get({
      url:'/api/shop/transport/page',
      method:'GET',
      params:query
   })
}

//自提点数据
export const _pickaddrList=(query?:any)=>{
    return request.get({
        url:'/api/shop/pickAddr/page',
        method:'GET',
        params:query
    })
}

//自提点删除
export const delPick=(data:any)=>{
    return request.delete({
        url:'/api/shop/pickAddr',
        method:'DELETE',
        data
    })
}

//获取自提点管理的地市数据
export const _getaddrList=(query:any)=>{
    return request.get({
        url:`/api/admin/area/listByPid`,
        params:query
    })
}

//新增自提点管理
export const _addpicktList=(data:any)=>{
    return request.post({
        url:'/api/shop/pickAddr',
        data
    })
}

//编辑自提点管理
export const _updatepicktList=(data:any)=>{
   return request.put({
        url:'/api/shop/pickAddr',
        data
   })
}