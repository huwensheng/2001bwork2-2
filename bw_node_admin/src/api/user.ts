import request from '../utils/index'

interface addrtype {
    data: any;
    current: number;
    size: number;
  }
export const vipList = (params:any) =>
  request.get<addrtype>({
    url:`/api/admin/user/page`,
    method: 'GET',
    params:params
  })

//编辑
export const vipedit=(data:any)=>{
    return request.put({
        url:'/api/admin/user',
       data
    })
}