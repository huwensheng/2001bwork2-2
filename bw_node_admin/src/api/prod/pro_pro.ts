// 分类管理

import request  from "@/utils";

export const getProd = (params: any) =>
  request.get({
    url: "/api/prod/prod/page",
    method: "get",
    params,
  });
export const getProdInfo = (id: number) =>
  request.get({
    url: `/api/prod/prod/info/${id}`,
    method: "get",
  });
export const saveProd = (data: any) =>
  request.post({
    url: "/api/prod/prod",
    method: "post",
    data,
  });
export const editProd = (data: any) =>
  request.put({
    url: "/api/prod/prod",
    method: "put",
    data,
  });
export const delProd = (data: number[]) =>
  request.delete({
    url: "/api/prod/prod",
    method: "delete",
    data,
  });
export const getProdStatus = (data: any) =>
  request.put({
    url: "/api/prod/prod/prodStatus",
    method: "put",
    data,
  });
