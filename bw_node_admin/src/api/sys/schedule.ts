import request from "../../utils/index";
// 定时任务
export const get_scheduleList = (query?: any) => request.get({
    url: "/api/sys/schedule/page",
    method: "GET",
    params: query
})
// 删除  /sys/schedule
export const del_scheduleList = (data: any) => request.delete({
    url: "/api/sys/schedule",
    method: "DELETE",
    data
})
// 添加  
export const post_scheduleList = (data: any) => request.post({
    url: "/api/sys/schedule",
    method: "POST",
    data
})
// 编辑   /sys/schedule
export const put_scheduleList = (data: any) => request.put({
    url: "/api/sys/schedule",
    method: "PUT",
    data
})
// 暂停  
export const post_pause = (data: any) => request.post({
    url: "/api/sys/schedule/pause",
    method: "POST",
    data
})
// 恢复
export const post_resume = (data: any) => request.post({
    url: "/api/sys/schedule/resume",
    method: "POST",
    data
})
// 立即执行
export const post_run = (data: any) => request.post({
    url: "/api/sys/schedule/run",
    method: "POST",
    data
})