import request from '../../utils'
// 管理员列表  
export const get_userList = (query?: any) => request.get({
    url: "/api/sys/user/page",
    method: "GET",
    params: query
})
// 编辑
export const put_userList = (data: any) => request.put({
    url: "/api/sys/user",
    method: "PUT",
    data
})
// 添加
export const post_userList = (data: any) => request.post({
    url: "/api/sys/user",
    method: "POST",
    data
})
// 删除
export const delete_userList = (data: any) => request.delete({
    url: `/api/sys/user`,
    method: "DELETE",
    data
})
