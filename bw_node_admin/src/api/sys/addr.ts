import request from '../../utils'
// 地址管理

export const getAddress = (params: any) =>
 request.get({
    url: "/api/admin/area/list",
    method: "GET",
    params,
  });
//   // 编辑
export const editAddress = (data: any) =>
 request.put({
    url: "/api/admin/area",
    method: "PUT",
    data,
  });
// //  删除
export const delAddress = (id: any) =>
  request.delete({
    url: `/api/admin/area/${id}`,
    method: "DELETE",
  });
//   // 添加
//   export const addAddress=(data:any)=>{
//     request.post({
//       url:"/api/admin/area",
//       method:"POST",
//       data,
//     })
//   }
  export const getAddressInfo = (id: number) =>
  request.get({
    url: `/api/admin/area/info/${id}`,
    method: "GET",
  });

export const addAddress = (data: any) =>
  request.post({
    url: "/api/admin/area",
    method: "POST",
    data,
  });