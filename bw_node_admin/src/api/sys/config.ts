import request from '../../utils'
// 参数管理
export const get_config = (query?:any) => request.get({
    url: "/api/sys/config/page",
    method: "GET",
    params:query
})
// 编辑
export const put_config = (data: any) => request.put({
    url: "/api/sys/config",
    method: "PUT",
    data
})
// 添加
export const post_config = (data: any) => request.post({
    url: "/api/sys/config",
    method: "POST",
    data
})
// 删除
export const delete_config = (data: any) => request.delete({
    url: `/api/sys/config`,
    method: "DELETE",
    data
})