import request from '../../utils'
import {tableType} from '../../types/sys/loglist';
interface logType extends tableType{
    username?:string;
    operation?:string;
}
// 日志
export const get_setList=(params: logType)=>
     request.get({
        url:"/api/sys/log/page",
        method:"GET",
        params
    })

