import request from '../../utils'
// // 菜单
// export const get_menu = (query?:any) => request.get({
//     url: "/api/sys/menu/table",
//     method: "GET",
//     params:query
// })
// // 编辑
// export const put_menu = (data: any) => request.put({
//     url: "/api/sys/menu",
//     method: "PUT",
//     data
// })
// // 添加
// export const post_menu = (data: any) => request.post({
//     url: "/api/sys/menu",
//     method: "POST",
//     data
// })
// // 删除
// export const delete_menu = (menuId: any) => request.delete({
//     url: `/api/sys/menu/${menuId}`,
//     method: "DELETE",
//     // data
// })
//获取菜单数据
export const get_menu=()=>request.get({
        url:'/api/sys/menu/table',
        method:'GET'
    })

//删除
export const del_menu=(id:any)=> request.delete({
        url:`/api/sys/menu/${id}`,
        method:'DELETE'
    })

//新建
export const add_menu=(data:any)=> request.post({
        url:'/api/sys/menu',
        method:'POST',
        data
    })


//获取编辑数据
export const get_editmenu=(id:any)=> request.get({
        url:`/api/sys/menu/${id}`,
        method:'GET'
    })


//编辑
export const edit_menu=(data:any)=> request.put({
        url:`/api/sys/menu`,
        method:'PUT',
        data
    })

    export const get_menuList=()=> request.get({
            url:'/api/sys/menu/table',
            method:'GET'
        })