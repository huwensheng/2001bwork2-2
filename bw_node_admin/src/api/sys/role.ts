import request from "../../utils/index";
// 角色管理
export const get_roleList = (query?: any) => request.get({
    url: "/api/sys/role/page",
    method: "GET",
    params: query
})
// 编辑
export const put_role = (data: any) => request.put({
    url: "/api/sys/role",
    method: "PUT",
    data
})
// 添加
export const post_role = (data: any) => request.post({
    url: "/api/sys/role",
    method: "POST",
    data
})

// 删除
export const delete_role = (data: any) => request.delete({
    url: `/api/sys/role`,
    method: "DELETE",
    data
})