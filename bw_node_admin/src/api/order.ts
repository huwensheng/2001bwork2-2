import request from "../utils/index";
import { ResponseData, OrderListRes } from "../types/type"

// 获取订单列表页数据
export const _get_orderList = (params:any) => request.get<ResponseData<OrderListRes>>({
    url:`/api/order/order/page?current=1&size=10`,
    params:params
})