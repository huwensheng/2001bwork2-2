import request from "../utils/index";
import { ResponseData, LoginParams, userInfo, NavList } from "../types/type"

//获取导航列表
export const _get_menulist = (data?: any) =>
    request.get<ResponseData<NavList>>({
        url: `/api/sys/menu/nav`,
        data
    })

// 获取用户信息
export const _get_userinfo = (data?: any) => request.get<ResponseData<userInfo>>({
    url: `/api/sys/user/info`,
    data
})

export const _login = (params:any) => request.post<ResponseData<LoginParams>>({
    url:`/api/login?grant_type=admin`,
    data:params
})




