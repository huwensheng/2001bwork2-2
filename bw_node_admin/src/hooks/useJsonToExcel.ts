import { utils, writeFile, BookType } from "xlsx"

const useJsonToExcel = (options:{
    data:any[],
    header:any,
    fileName:string,
    bookType:BookType
}) => {
    // 创建工作簿
    const exSheet = utils.book_new();
    // 创建工作表
    if(options.header){
        // 转换data
        options.data = options.data.map(item => {
            // 定义一个对象
            const obj:any = {}
            for(const key in item){
                obj[options.header[key]] = item[key]
            }
            return obj
        })
    }
    const ts = utils.json_to_sheet(options.data)
    // 把生成的工作表放到工作簿
    utils.book_append_sheet(exSheet, ts);
    // 生成数据
    writeFile(exSheet,options.fileName,{
        bookType:options.bookType
    })
}

export default useJsonToExcel