
  export interface AddressType {
    areaId: number;
    areaName: string;
    areas?: AreaType[];
    level?: number;
    parentId?: number | null;
  }
  export interface AddressResType {
    current: number;
    pages: number;
    records: AddressType[];
    searchCount: boolean;
    size: number;
    total: number;
  }
  