export interface prodPropValuesType {
  valueId?: number;
  propValue: string;
  propId?: number;
}
export interface SpecsType {
  prodPropValues: prodPropValuesType[];
  propId?: number;
  propName: string;
  rule?: number;
  shopId?: number;
}
export interface SpecsResType {
  records: SpecsType[];
  total: number;
  searchCount: boolean;
  size: number;
  current: number;
  pages: number;
}
