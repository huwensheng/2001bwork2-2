// 规格管理

import  request  from "@/utils";

export const getSpecs = (params: any) =>
  request.get({
    url: "/api/prod/spec/page",
    method: "get",
    params,
  });
export const saveSpecs = (data: any) =>
  request.post({
    url: "/api/prod/spec",
    method: "post",
    data,
  });
export const editSpecs = (data: any) =>
  request.put({
    url: "/api/prod/spec",
    method: "put",
    data,
  });
export const delSpecs = (id: number) =>
  request.delete({
    url: `/api/prod/spec/${id}`,
    method: "delete",
  });
