// 分类管理

import  request  from "@/utils";

export const getCategory = (params: any) =>
  request.get({
    url: "/api/prod/category/table",
    method: "get",
    params,
  });
export const getCategoryInfo = (id: number) =>
  request.get({
    url: `/api/prod/category/info/${id}`,
    method: "get",
  });
export const saveCategory = (data: any) =>
  request.post({
    url: "/api/prod/category",
    method: "post",
    data,
  });
export const editCategory = (data: any) =>
  request.put({
    url: "/api/prod/category",
    method: "put",
    data,
  });
export const delCategory = (id: number) =>
  request.delete({
    url: `/api/prod/category/${id}`,
    method: "delete",
  });
