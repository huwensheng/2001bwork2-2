// 评论管理

import  request  from "@/utils";

export const getComment = (params: any) =>
  request.get({
    url: "/api/prod/prodComm/page",
    method: "get",
    params,
  });
export const getCommentItem = (id: number) =>
  request.get({
    url: `/api/prod/prodComm/info/${id}`,
    method: "get",
  });
export const saveComment = (data: any) =>
  request.post({
    url: "/api/prod/prodComm",
    method: "post",
    data,
  });
export const editComment = (data: any) =>
  request.put({
    url: "/api/prod/prodComm",
    method: "put",
    data,
  });
export const delComment = (id: number) =>
  request.delete({
    url: `/api/prod/prodComm/${id}`,
    method: "delete",
  });
