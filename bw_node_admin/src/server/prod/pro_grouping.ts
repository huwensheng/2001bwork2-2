// 分类管理

import request  from "@/utils";
import { GroupingReqType, GroupingType } from "@/views/grouping/types";

export const getGrouping = (params: GroupingReqType) =>
  request.get({
    url: "/api/prod/prodTag/page",
    method: "get",
    params,
  });
export const getGroupingInfo = (id: number) =>
  request.get({
    url: `/api/prod/prodTag/info/${id}`,
    method: "get",
  });
export const saveGrouping = (data: GroupingType) =>
  request.post({
    url: "/api/prod/prodTag",
    method: "post",
    data,
  });
export const editGrouping = (data: GroupingType) =>
  request.put({
    url: "/api/prod/prodTag",
    method: "put",
    data,
  });
export const delGrouping = (id: number) =>
  request.delete({
    url: `/api/prod/prodTag/${id}`,
    method: "delete",
  });
