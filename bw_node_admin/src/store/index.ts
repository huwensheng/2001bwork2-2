import useLoginStore from "./module/login"
import usesysStore from './module/sys/sysStore'
import usesysconfigStore from './module/sys/config'
import usesysroleStore  from './module/sys/role'
import usesysuserStore  from './module/sys/user'
import usesysscheduleStore  from './module/sys/schedule'
import usesysmenuStore  from './module/sys/menu'
import useOrderStore from "./module/order"
import useShopStore from "./module/shop"
import useVip from "./module/vip"

export const getMenuListAgain = () => {
    console.log(useLoginStore());
}


export default function useStore () {
    return {
        login : useLoginStore(),
        order : useOrderStore(),
        shops:useShopStore(),
        sysStore:usesysStore(),
        sysConfigStore:usesysconfigStore(),
        sysRoleStore:usesysroleStore(),
        sysUserStore:usesysuserStore(),
        sysscheduleStore:usesysscheduleStore(),
        sysmenuStore:usesysmenuStore(),
        vip:useVip()
    }
}  