import { defineStore } from "pinia"
import { _get_orderList } from "../../api/order"
import cache from "../../utils/util"



const order = defineStore('order',{
    state () {
        return {
            orderList: cache.getCache('orderList') || []
        }
    },
    actions : {
        async get_orderList(payload:any){
            console.log(payload);
            const {data} = await _get_orderList(payload)
            // console.log(data.records,"订单数据");
            this.orderList = data.records
            cache.setCache('orderList',data.records)
        }
    }
})

export default order