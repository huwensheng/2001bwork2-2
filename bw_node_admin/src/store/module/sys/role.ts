import { defineStore } from 'pinia'
import { get_roleList } from '../../../api/sys/role'
const sysRoleStore: any = defineStore('sysRoleStore', {
    state() {
        return {
            roleList: [],
            roleDeatil: {},
            roleSearch: {},//搜索
            roleCurrent: 1,
            roleSize: 10,
        }
    },
    actions: {
        // 数据
        async getrole() {
            const res: any = await get_roleList({
                current: this.roleCurrent,
                size: this.roleSize
            })
            this.roleList = res.data.records
            this.roleDeatil = res.data
        },
        // 搜索
        async searchRole(row: any) {
            console.log(row);
            const res: any = await get_roleList({
                current: this.roleCurrent,
                size: this.roleSize,
                roleName: row.roleName
            })
            this.roleList = res.data.records//再次渲染
        },
        //清空
        async clear(row: any) {
            row.roleName = ''
        },
        //  //分页
        //  roleSizeChange(page: number) {
        //     this.roleCurrent=page
        //     // this.roleCurrent = 1
        //     // this.roleSize = page
        //     this.getrole()
        // },
        // roleCurrentChange(page: number) {
        //       this.roleSize=page
        //       this.roleCurrent=1
        //     // this.roleCurrent = page
        //     this.getrole()
        // },

    }
})
export default sysRoleStore