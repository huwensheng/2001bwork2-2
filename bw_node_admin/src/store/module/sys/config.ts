import { defineStore } from 'pinia'
import {
    get_config,//数据
    delete_config,//删除 
} from '../../../api/sys/config'
const sysConfigStore: any = defineStore('sysConfigStore', {
    state() {
        return {
            configList: [],//参数管理数据
            configDeatil: {},
            configSearch: {},//搜索 
            configCurrent: 1,
            configSize: 10,
        }
    },
    actions: {
        // 数据
        async getconfig() {
            const res: any = await get_config({
                current: this.configCurrent,
                size: this.configSize
            })
            this.configList = res.data.records//渲染
            this.configDeatil = res.data.total
        },
        // 搜索
        async searchConfig(row: any) {
            console.log(row);//打印输入框的数据
            const res: any = await get_config({
                current: this.configCurrent,
                size: this.configSize,
                paramKey: row.paramKey
            })
            this.configList = res.data.records//再次渲染
        },
        //清空
        async clear(row: any) {
            row.paramKey = ''
        },
        //分页
        configSizeChange(page: number) {
            this.configCurrent = 1
            this.configSize = page
            this.getconfig()
        },
        configCurrentChange(page: number) {
            //   this.userCurrent=1
            this.configCurrent = page
            this.getconfig()
        },
    }
})
export default sysConfigStore