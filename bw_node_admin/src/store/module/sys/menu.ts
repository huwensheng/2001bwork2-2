import { defineStore } from 'pinia'
import { get_menu, add_menu, edit_menu, del_menu, get_menuList } from '../../../api/sys/menu'
import { treeDataTranslate } from '../../../utils/tool'
const sysmenuStore: any = defineStore('sysmenuStore', {
    state() {
        return {
            // 管理员列表
            menuList: [],

            //菜单管理
            menudata: [],
            menudataForm: [],
            menudialogVisible: false,
            menuFormData: {
                menuradio: '1',
                name: '',
                parentId: 0,
                icon: '',
                url: '',
                perms: '',
                orderNum: 0
            },
            EditmenudialogVisible: false,
            EditmenuFormData: {
                menuradio: '1',
                name: '',
                parentId: 0,
                icon: '',
                url: '',
                perms: '',
                orderNum: 0,
                menuId: 0
            },
        }
    },
    actions: {

     
        //菜单数据
        async GetenuList() {
            const res: any = await get_menu()
            this.menudata = treeDataTranslate(res.data, 'menuId', 'parentId')
        },
        async DelMenu(id: any) {
            await del_menu(id)
            this.GetenuList()
        },
        //新增菜单
        async addMenu() {

        },
        handleSelectMenuChange(val: any) {
            this.menuFormData.parentId = val[val.length - 1]
        },
        async menuAddTrue() {
            if (this.menuFormData.menuradio == '0') {
                await add_menu(
                    {
                        icon: '',
                        name: this.menuFormData.name,
                        orderNum: this.menuFormData.orderNum,
                        parentId: this.menuFormData.parentId,
                        perms: this.menuFormData.perms,
                        type: 0,
                        url: this.menuFormData.url
                    }
                )
            } else if (this.menuFormData.menuradio == '1') {
                await add_menu(
                    {
                        icon: '',
                        name: this.menuFormData.name,
                        orderNum: this.menuFormData.orderNum,
                        parentId: this.menuFormData.parentId,
                        perms: this.menuFormData.perms,
                        type: 1,
                        url: this.menuFormData.url
                    }
                )
            } else {
                await add_menu(
                    {
                        icon: '',
                        name: this.menuFormData.name,
                        orderNum: this.menuFormData.orderNum,
                        parentId: this.menuFormData.parentId,
                        perms: this.menuFormData.perms,
                        type: 2,
                        url: this.menuFormData.url
                    }
                )
            }

            this.menudialogVisible = false
            this.GetenuList()
        },
        async getMenuEditList(row: any) {
            this.EditmenudialogVisible = true
            console.log(row)
            if (row.type === 0) {
                this.EditmenuFormData = {
                    menuradio: '0',
                    name: row.name,
                    parentId: row.parentId,
                    icon: row.icon,
                    url: row.url,
                    perms: row.perms,
                    orderNum: row.orderNum,
                    menuId: row.menuId
                }
            } else if (row.type === 1) {
                this.EditmenuFormData = {
                    menuradio: '1',
                    name: row.name,
                    parentId: row.parentId,
                    icon: row.icon,
                    url: row.url,
                    perms: row.perms,
                    orderNum: row.orderNum,
                    menuId: row.menuId
                }
            } else {
                this.EditmenuFormData = {
                    menuradio: '2',
                    name: row.name,
                    parentId: row.parentId,
                    icon: row.icon,
                    url: row.url,
                    perms: row.perms,
                    orderNum: row.orderNum,
                    menuId: row.menuId
                }
            }
        },
        async menuEditTrue() {
            if (this.EditmenuFormData.menuradio == '0') {
                await edit_menu({
                    icon: this.EditmenuFormData.icon,
                    name: this.EditmenuFormData.name,
                    orderNum: this.EditmenuFormData.orderNum,
                    parentId: this.EditmenuFormData.parentId,
                    perms: this.EditmenuFormData.perms,
                    type: 0,
                    url: this.EditmenuFormData.url,
                    menuId: this.EditmenuFormData.menuId
                })
            } else if (this.EditmenuFormData.menuradio == '1') {
                await edit_menu({
                    icon: this.EditmenuFormData.icon,
                    name: this.EditmenuFormData.name,
                    orderNum: this.EditmenuFormData.orderNum,
                    parentId: this.EditmenuFormData.parentId,
                    perms: this.EditmenuFormData.perms,
                    type: 1,
                    url: this.EditmenuFormData.url,
                    menuId: this.EditmenuFormData.menuId
                })
            } else {
                await edit_menu({
                    icon: this.EditmenuFormData.icon,
                    name: this.EditmenuFormData.name,
                    orderNum: this.EditmenuFormData.orderNum,
                    parentId: this.EditmenuFormData.parentId,
                    perms: this.EditmenuFormData.perms,
                    type: 2,
                    url: this.EditmenuFormData.url,
                    menuId: this.EditmenuFormData.menuId
                })
            }

            this.EditmenudialogVisible = false
            this.GetenuList()
        },
           // 菜单管理
           async get_menuList() {
            const res: any = await get_menuList()
            this.menuList = res.data
        },

    }
})
export default sysmenuStore