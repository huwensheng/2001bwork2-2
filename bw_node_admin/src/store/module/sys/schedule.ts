import { defineStore } from 'pinia'
import {get_scheduleList} from '../../../api/sys/schedule'

const sysscheduleStore: any = defineStore('sysscheduleStore', {
    state() {
        return {
            scheduleList: [],//参数管理数据
            scheduleDeatil: {},
            scheduleSearch: {},//搜索 
            scheduleCurrent: 1,
            scheduleSize: 10,
        }
    },
    actions: {
        // 数据
        async getschedule() {
            const res: any = await get_scheduleList({
                current: this.scheduleCurrent,
                size: this.scheduleSize
            })
            this.scheduleList = res.data.records//渲染
            this.scheduleDeatil = res.data.total
        },
        // 搜索
        async searchschedule(row: any) {
            console.log(row);//打印输入框的数据
            const res: any = await get_scheduleList({
                current: this.scheduleCurrent,
                size: this.scheduleSize,
                beanName: row.beanName
            })
            this.scheduleList = res.data.records//再次渲染
        },
        //清空
        async clear(row: any) {
            row.beanName = ''
        },
        //分页
        scheduleSizeChange(page: number) {
            this.scheduleCurrent = 1
            this.scheduleSize = page
            this.getschedule()
        },
        scheduleCurrentChange(page: number) {
            //   this.userCurrent=1
            this.scheduleCurrent = page
            this.getschedule()
        },
    }
})
export default sysscheduleStore