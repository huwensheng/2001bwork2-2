import { defineStore } from 'pinia'
import {
    get_userList,//数据
    delete_userList//删除
} from '../../../api/sys/user'
const sysUserStore: any = defineStore('sysUserStore', {
    state() {
        return {
            userList: [],//参数管理数据
            userDeatil: {},
            userSearch: {},//搜索
            userCurrent: 1,
            userSize: 10,
        }
    },
    actions: {
        // 渲染
        async getuser() {
            const res: any = await get_userList({
                current: this.userCurrent,
                size: this.userSize
            })
            this.userList = res.data.records
            this.userDeatil = res.data.total
        },
        // 搜索
        async searchUser(row: any) {
            console.log(row);
            const res: any = await get_userList({
                current: this.userCurrent,
                size: this.userSize,
                username: row.username
            })
            this.userList = res.data.records
        },
        //清空
        async clear(row: any) {
            row.username = ''
        },

        // sizeChange (val) {
        //     this.page.currentPage = 1 //  currentPage: 1,   pageSize: 10
        //     this.page.pageSize = val
        //     this.getList()
        //     this.$message.success('行数' + val)
        //   },
        //   currentChange (val) {
        //     this.page.currentPage = val
        //     this.getList()
        //     this.$message.success('页码' + val)
        //   }
        //分页
        userSizeChange(page: number) {
            this.userCurrent = 1
            this.userSize = page
            this.getuser()
        },
        userCurrentChange(page: number) {
            //   this.userCurrent=1
            this.userCurrent = page
            this.getuser()
        },

    }
})
export default sysUserStore