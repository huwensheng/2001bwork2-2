import { defineStore } from 'pinia'
import {DelshopList, //公告删除
        getShopList, //公告数据
        shopHotList, //热搜数据
        indexList,//轮播数据
        swiperlist,//轮播确认数据
        _transportList,//运费数据
        _pickaddrList,//自提点数据
      
      } from "../../api/shop"
const shop:any=defineStore('shopstore',{
     state(){
        return {
          noticeList:[], //公告管理
          shopList:[],//热搜数据
          noticeDeatil:{},
          shopSearch:{},//公告搜索
          hotSearch:{},//热搜搜索
          delid:null,
          noticeCurrent:1,
          noticeSize:10,
          hotCurrent:1,
          hotSize:10,
          hotDeatil:{},
          indexCurrent:1,
          indexSize:10,
          indexDeatil:{},
          tranCurrent:1,
          tranSize:10,
          tranDeatil:{},
          pickCurrent:1,
          pickSize:10,
          pickDeatil:{},
          indeximgList:[],//轮播图数据
          indexImgSearch:{},//轮播搜索
          swiperList:[],
          transportList:[],//运费数据
          transportSearch:{},//搜索数据
          pickaddrList:[],//自提点数据
          pickaddrSearch:{},//自提点搜索
       
        }
     },
     actions:{
      //数据渲染
       async getShopList(){
        const res:any=await getShopList({
          current:this.noticeCurrent,
          size:this.noticeSize
        })
        this.noticeList=res.data.records
        this.noticeDeatil=res.data.total
        console.log(this.noticeList)
       },
      
       //搜索
       async searchshopList(row:any){
        console.log(row)
        if(row.status=='撤销'){
            const res:any=await getShopList({
                current:this.noticeCurrent,
                size:this.noticeSize,
                title:row.title,
                status:0
            })
            this.noticeList=res.data.records
        }else if(row.status==="公布"){
           const res:any=await getShopList({
             current:this.noticeCurrent,
            size:this.noticeSize,
            title:row.title,
            status:1
           })
           this.noticeList=res.data.records
        }  else if(row.isTop==="否"){
          const res:any=await getShopList({
            current:this.noticeCurrent,
            size:this.noticeSize,
            title:row.title,
            isTop:0
          })
          this.noticeList=res.data.records
      }else if(row.isTop==="是"){
         const res:any=await getShopList({
          current:this.noticeCurrent,
          size:this.noticeSize,
          title:row.title,
          isTop:1
         })
         this.noticeList=res.data.records
      }else{
        const res:any=await getShopList({
          current:this.noticeCurrent,
          size:this.noticeSize,
           title:row.title  
           })
          this.noticeList=res.data.records
          }
      },
      //清空
      async clear(row:any){
           row.status=''
           row.isTop=''
           row.title=''
      },
      //分页
     
     //热搜数据
      async hotList(){
        const res:any=await shopHotList({
          current:this.hotCurrent,
          size:this.hotSize
        })
        this.noticeList=res.data.records
        this.hotDeatil=res.data.total
        this.shopList=res.data.records
        console.log(this.shopList)
      },
    //热搜搜索
    async searchhotList(row:any){
      console.log(row)
      if(row.status==='启用'){
          const res:any=await shopHotList({
              title:row.title,
              content:row.content,
              recDate:row.recDate,
              seq:row.seq,
              status:1
          })
          console.log(res)
          this.shopList=res.data.records
      }else if(row.status==="未启用"){
         const res:any=await shopHotList({
          content:row.content, 
          title:row.title,
          recDate:row.recDate,
          seq:row.seq,
          status:0
         })
         this.shopList=res.data.records
      } else{
      const res:any=await shopHotList({
         content:row.content, 
         title:row.title,
         recDate:row.recDate,
          seq:row.seq,
         })
        console.log(res.hotSearch);
        this.shopList=res.data.records
      }  
   
    },


    //轮播数据
    async getIndexList(){
      const res:any=await indexList({
          current:this.indexCurrent,
          size:this.indexSize
      })
      this.indexDeatil=res.data
      this.indeximgList=res.data.records
     },
     async swiper(){
      const res:any=await swiperlist({

      })
     this.swiperList=res.data.records 
     console.log(this.swiperList,'商品选择数据')
    },
     //轮播搜索
     async indexSearch(row:any){
       console.log(row)
     if(row.status==="正常"){
      const res:any=await indexList({
         imgUrl:row.imgUrl,
         seq:row.seq,
         status:1
      })
      console.log(res)
      this.indeximgList=res.data.records
     }else if(row.status==="禁用"){
      const res:any=await indexList({
         imgUrl:row.imgUrl,
         seq:row.seq,
         status:0
      })
      console.log(res)
      this.indeximgList=res.data.records
     }else{
        const res:any=await indexList({
          imgUrl:row.imgUrl,
          seq:row.seq,
          status:0
        })
        this.indeximgList=res.data.records
     }
  },
  //清空
  async indeximgclear(row:any){
    row.status=''
  },
  //运费数据
  async getTransportList(){
    const res:any=await _transportList({
      current:this.tranCurrent,
      size:this.tranSize
    })
    this.tranDeatil=res.data
    this.transportList=res.data.records
  },
  //运费搜索
  async searchTransportList(row:any){
      const res:any=await _transportList({
        transName:row.transName
      })
      this.transportList=res.data.records
  },
  //运费清空
  async transportclear(row:any){
    row.transName=''
  },
  //自提点数据
  async getPickaddrList(){
    const res:any=await _pickaddrList({
      current:this.pickCurrent,
      size:this.pickSize
    })
    this.pickDeatil=res.data.total
    this.pickaddrList=res.data.records
  },
  async searchpickList(row:any){
    const res:any=await _pickaddrList({
      addrName:row.addrName,
      mobile:row.mobile,
      province:row.province,
      city:row.city,
      area:row.area,
      addr:row.addr
    })
    this.pickaddrList=res.data.records
},
    async pickclear(row:any){
      row.addrName=''
    },
   
   
}
})

export default shop