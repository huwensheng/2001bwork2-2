import { defineStore } from 'pinia'
import { _login, _get_userinfo, _get_menulist } from "../../api/login"
import cache from "../../utils/util"
import { getRoutes } from "../../utils/permission"
import router from "../../router/index"
import { ElMessage } from 'element-plus'


const login = defineStore('login',{
    state() {
        return {
            token: cache.getCache('token') || '',
            authorities: cache.getCache('authorities') || [],
            userInfo : cache.getCache('userinfo') || [],
            menuList : cache.getCache('menuList') || []
        }
    },
    actions: {
        async loginAction (payload:any) {
            const res = await _login(payload)
            // console.log(res);
            try {
                // console.log(router,'路由');
                cache.setCache('token',`${res.data.token_type}${res.data.access_token}`)
                cache.setCache('authorities',res.data.authorities)
                this.token = `${res.data.token_type}${res.data.access_token}`
                this.authorities = res.data.authorities
                // 获取用户信息
                const userInfo = await _get_userinfo();
                // console.log(userInfo);
                this.userInfo = userInfo.data
                cache.setCache('userInfo',userInfo.data)
                // 获取导航列表
                const menuList = await _get_menulist()
                // console.log(menuList);
                cache.setCache('menuList',menuList.data.menuList)
                this.menuList = menuList.data.menuList
                // this.changeMenu(menuList.data.menuList)
                router.push("/index")
            } catch (error) {
                console.log(error);
                ElMessage.error(`${error}`)
            }
        },
        changeMenu (payload:any) {
            // const routes = getRoutes(payload)
            // // 动态添加到路由表里面
            // // addRoutes  addRoute
            // // addRoute 路由中的api 是增加一级路由  如果要增加二级路由一定要带上父路由地址
            // routes.forEach(item => {
            //     console.log(item,'二级动态路由添加');
                
            //     router.addRoute('home',item)
            // })
        },
        getMenu(){
            // 防止刷新的时候丢失menu数据
            // 刷新的时候 重新获取menu 从本地
            // const menuList = cache.getCache("menuList");
            // const authorities = cache.getCache('authorities');
            // if(menuList) {
            //     this.menuList = [...menuList];
            //     this.authorities = [...authorities]
            //     // 动态添加路由
            //     this.changeMenu([...menuList])
            // }
        }
    }
})

export default login