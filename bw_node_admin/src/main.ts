import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import router from "./router/index"
import { createPinia } from "pinia"
import * as ElementPlusIconsVue  from "@element-plus/icons-vue"
import Avue from '@smallwei/avue';
import '../node_modules/@smallwei/avue/lib/index.css';
// import useLogin from "./store/index"

const app = createApp(App)
// const { login } = useLogin()

for ( const [key, component] of Object.entries(ElementPlusIconsVue) ) {
    app.component(key, component)
}

app.use(router).use(ElementPlus).use(createPinia()).use(Avue).mount('#app')

// login.getMenu()


