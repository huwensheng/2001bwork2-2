# Vue 3 + TypeScript + Vite

This template should help get you started developing with Vue 3 and TypeScript in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar)

## Type Support For `.vue` Imports in TS

Since TypeScript cannot handle type information for `.vue` imports, they are shimmed to be a generic Vue component type by default. In most cases this is fine if you don't really care about component prop types outside of templates. However, if you wish to get actual prop types in `.vue` imports (for example to get props validation when using manual `h(...)` calls), you can enable Volar's Take Over mode by following these steps:

1. Run `Extensions: Show Built-in Extensions` from VS Code's command palette, look for `TypeScript and JavaScript Language Features`, then right click and select `Disable (Workspace)`. By default, Take Over mode will enable itself if the default TypeScript extension is disabled.
2. Reload the VS Code window by running `Developer: Reload Window` from the command palette.

You can learn more about Take Over mode [here](https://github.com/johnsoncodehk/volar/discussions/471).


### 检测提交的记录是否符合规范 commitlint
    ---下载 @commitlint/cli @commit/config-conventional 这两个插件
    ---新建commitlint.config.js 在配置文件中写入自定义配置信息
    ---执行 npx husky add .husky/commit-msg 'npx --no -- commitlint --edit ${1}' 在.husky目录中生成commit-msg


### rbac模型
    --- rbac (全称 Role-Based Access Control：基于角色的访问控制)
    --- rbac 权限授权 抽象的理解为 who 是否可以对what进行how的访问操作，并且对这个逻辑表达式进行判断 是否为true的过程， 也就是将权限问题转为what  how 的问题
    --- rabc 用户、角色、权限三部分组成
    --- rbac 是通过对角色权限的定义，对用户授予某个角色从而控制用户的权限，实现了 用户和逻辑的分离
    --- User 用户 ：每一个用户都有一个唯一的id 并且被授予不同的角色
    --- Role 角色 ：不同的角色具有不同的权限
    --- permission 权限 ： 访问的权限
    --- 用户-角色映射 ：用户和角色之间的关系
    --- 角色-权限的映射 ：角色和权限之间的关系

### rbac的原则和优点
    --- 著名的安全原则问题 ：最小的权限原则，责任分离原则和数据抽象
        --- 最小权限原则 ：rbac 能将角色配置其完成任务所需的最小权限集合
        --- 责任分离原则 ：通过调用互相独立互相排斥的角色来共同完成敏感的任务
        --- 数据抽象原则 ：通过权限的抽象来体现
    --- 优点：
        --- 简化了用户权限关系
        --- 易扩展 易维护
    --- 缺点：
        ——— 没有控制次序
